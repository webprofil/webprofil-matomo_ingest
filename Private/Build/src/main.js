// @ts-check

class MatomoUtils {
    /** @type {MatomoUtils|null} */
    static __instance = null;

    constructor() {
        if (MatomoUtils.__instance) return MatomoUtils.__instance;
        MatomoUtils.__instance = this;

        window.addEventListener('load', () => {
            this.addEventListeners();
        });
    }

    addEventListeners() {
        document.querySelectorAll('a[data-mt-link-event]').forEach((element) => {
            element.addEventListener('click', async (event) => {
                event.preventDefault();

                const href = element.getAttribute('href') || '';
                const target = element.getAttribute('target') || '_self';

                const matomoEventName = element.getAttribute('data-mt-link-event') ?? '';

                if (target === '_blank') {
                    window.open(href, target);
                    await this.triggerEvent(matomoEventName, href);
                } else {
                    await this.triggerEvent(matomoEventName, href);
                    window.location.href = href;
                }
            });
        });
    }

    /**
     * @param {"sponsors"|string} eventName
     * @param {string} data
     */
    async triggerEvent(eventName, data) {
        const dataObj = {
            eventName: eventName,
            data: data,
        };

        return await fetch("?type=819328686163324386", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(dataObj),
        }).then((response) => {
            return response.json();
        }).catch((error) => {
            console.error('Error:', error);
        });
    }
}

globalThis.WpMatomoUtils = new MatomoUtils();
