import { defineConfig } from 'vite';
import autoprefixer from 'autoprefixer'

export default defineConfig({
    css: {
        postcss: {
            plugins: [
                autoprefixer({})
            ],
        },
        devSourcemap: true,
    },
    build: {
        minify: true,
        sourcemap: true,
        target: ['es2020', 'chrome58', 'firefox57', 'safari11'],
        cssCodeSplit: false,

        rollupOptions: {
            input: 'src/main.js',
            output: {
                dir: '../../Resources/Public/Js',
                entryFileNames: 'script.min.js',
                format: 'es',
                assetFileNames: (file) => {
                    if (file.name == "style.css") return 'style.min.css';
                    return '[name].[ext]';
                }
            },
        },
    },
});