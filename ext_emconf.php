<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Matomo Ingest',
    'description' => 'null',
    'category' => '',
    'author' => '',
    'author_email' => '',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
