# webprofil/matomo-ingest

- Git-Repo: <https://bitbucket.org/webprofil/webprofil-matomo_ingest/src/main/>
- Packagist: <https://packagist.org/packages/webprofil/matomo-ingest>

## Features

- Ingest server logs into Matomo
- Backend Module for viewing Matomo logs
- Automatically creates a Matomo site and a User based on the current config
- Track events such as clicks on external link via Events

## Installation

```bash
composer require webprofil/matomo-ingest
```

## Configuration / Extension Setup

### Settings (Ext Conf. / ENV)

authToken (required)
> Matomo Auth Token, is overwritten by env > MATOMO_AUTH_TOKEN

authTokenDisableOverride
> Disable Matomo Auth Token env override

matomoUrl
> Matomo Site URL, is overwritten by env > MATOMO_URL

matomoUrlDisableOverride
> Disable Matomo Site URL env override

siteUrl
> Matomo Site URL, is overwritten by env > MATOMO_SITE_URL

siteUrlDisableOverride
> Disable Matomo Site URL env override

siteIdOverride
> The Matomo Site ID is normally set automatically via the siteUrl, this setting allows to override it. Should only be used if a Matomo Site already exists

slackWebhook
> Sends a message to a Slack Webhook after the CLI command has been executed

trackEvents
> Allowed tracking event names, (comma separated list)

### Tack events

Prerequisite

- include the TypoScript template from this extension
- add the event name to the trackEvents setting, this acts like a whitelist

External links

- set a[data-mt-link-event] to the event name

```html
<f:link.typolink parameter="{<link params>}" additionalAttributes="{data-mt-link-event:'{<event name>}'}">
  {<link text>}
</f:link.typolink>
```

Trigger event via Js

```javascript
WpMatomoUtils.triggerEvent(eventName, data);
```

### Cronjob / CLI

```bash
typo3 WEBprofil/MatomoIngest:import
```

Import the logs into Matomo from the previous day.

Cannot be run directly from the Scheduler, use 'Run task on next cron job' instead.

## Packagist / Release Version 12.x.x

- 12.0.0 initial release 12.0.0
- 12.0.1 added LOG_PATH_FALLBACK
- 12.0.2 added t3monitoring wildcard to exclude-path
- 12.0.3 added track events, load site url on BE Module request
- 12.0.4 import all available logs, import logs with temporary identifier if site url is not available
- 12.0.5 cleanup, removed extension builder settings
- 12.0.6 removed site url checks since its always set (random val / settings / sys_registry)
- 12.0.7 added env:MATOMO_DISABLED check
- 12.0.8 added ext conf -> ignoreList

## ToDo

## Future plans

Custom ignore settings for cli import

Tracking events: store + bulk send events

Matomo Plugin for hiding not needed navigation items / settings

disable iframe settings matomo config

> enable_framed_settings <https://matomo.org/faq/troubleshooting/faq_147/>
