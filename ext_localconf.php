<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use WEBprofil\MatomoIngest\Controller\AjaxController;

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(static function () {
    $key = 'matomo_ingest';
    $name = 'MatomoIngest';

    ExtensionUtility::configurePlugin(
        $name,
        'AjaxRequestHandler',
        [
            AjaxController::class => 'track',
        ],
        // non-cacheable actions
        [
            AjaxController::class => 'track',
        ]
    );
});
