<?php
defined('TYPO3') or die();

call_user_func(static function () {
    $extensionKey = 'matomo_ingest';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'Matomo Ingest'
    );
});
