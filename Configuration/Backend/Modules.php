<?php

return [
    'matomo_analytics' => [
        'parent' => 'web',
        'access' => 'user',
        'workspaces' => 'live',
        'path' => '/module/WEBprofil/matomo_analytics',
        'labels' => ['title' => 'Analytics'],
        'extensionName' => 'MatomoIngest',
        'iconIdentifier' => 'matomo-ingest-be-logo',
        'navigationComponent' => '',
        'navigationComponentId' => '',
        'inheritNavigationComponentFromMainModule' => false,
        'controllerActions' => [
            \WEBprofil\MatomoIngest\Controller\BeModuleController::class => [
                'show',
            ],
        ],
    ],
];
