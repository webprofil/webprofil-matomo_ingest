<?php

return [
    'matomo-ingest-be-logo' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:matomo_ingest/Resources/Public/Icons/matomo-logo.svg',
    ],
];
