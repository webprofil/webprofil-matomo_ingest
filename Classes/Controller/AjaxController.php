<?php

/**
 * @noinspection UnknownInspectionInspection
 */

namespace WEBprofil\MatomoIngest\Controller;

use JsonException;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use WEBprofil\MatomoIngest\Services\Matomo;

class AjaxController extends ActionController
{
    protected Matomo $matomo;
    protected Registry $registry;

    /**
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws JsonException
     */
    public function __construct() {
        $this->matomo = GeneralUtility::makeInstance(Matomo::class);
        $this->registry = GeneralUtility::makeInstance(Registry::class);

        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        $this->matomo->init(true);
    }

    /** @noinspection PhpUnused */
    /**
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws JsonException
     */
    public function trackAction(): ResponseInterface
    {
        /** @noinspection DuplicatedCode */
        /** @noinspection NullPointerExceptionInspection */
        $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('matomo_ingest');
        $allowedEventNames = array_map('trim', explode(',', $extConf['trackEvents']));

        $reqData = json_decode(file_get_contents('php://input'), true, 512, JSON_THROW_ON_ERROR);

        $eventName = trim($reqData['eventName'] ?? '');
        $data = trim($reqData['data'] ?? '');

        $_sendTrackEvent = true;

        if ($eventName === '' || $data === '' || !in_array($eventName, $allowedEventNames, true)) {
            $_sendTrackEvent = false;
        }

        if ($_sendTrackEvent) {
            $this->matomo->tracker->doTrackEvent($eventName, $data);
        }

        if ($GLOBALS['BE_USER']) {
            return $this->jsonResponse(json_encode([
                'status' => 'done',
                'sendTrackEvent' => $_sendTrackEvent,
                'eventName' => $eventName,
                'data' => $data,
            ], JSON_THROW_ON_ERROR));
        }

        return $this->jsonResponse(json_encode(['status' => 'done'], JSON_THROW_ON_ERROR));
    }
}
