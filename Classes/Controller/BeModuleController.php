<?php

/**
 * @noinspection DuplicatedCode
 * @noinspection UnknownInspectionInspection
 */

namespace WEBprofil\MatomoIngest\Controller;

use JsonException;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use WEBprofil\MatomoIngest\Services\Matomo;
use WEBprofil\MatomoIngest\Services\Modules\Http;

class BeModuleController extends ActionController
{
    protected Matomo $matomo;
    protected Registry $registry;

    /**
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws JsonException
     */
    public function __construct(
        private readonly ModuleTemplateFactory $moduleTemplateFactory
    ) {
        $this->matomo = GeneralUtility::makeInstance(Matomo::class);
        $this->registry = GeneralUtility::makeInstance(Registry::class);

        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        $this->matomo->init();
    }

    /** @noinspection PhpUnused */
    public function showAction(): ResponseInterface
    {
        $password = $this->registry->get(Matomo::REGISTRY_NAMESPACE, Matomo::REGISTRY_PASSWORD);

        /** @noinspection SpellCheckingInspection */
        $this->view->assign('iframeUrl', Http::buildRequestUrl(
            $this->matomo->matomoUrl, [
                'module' => 'Login',
                'action' => 'logme',
                'login' => $this->matomo->siteDomain,
                'password' => $password,
            ]
        ));

        $this->view->assign('siteId', $this->matomo->siteId);

        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        $moduleTemplate->getDocHeaderComponent()->disable();
        $moduleTemplate->setContent($this->view->render());
        return $this->htmlResponse($moduleTemplate->renderContent());
    }
}
