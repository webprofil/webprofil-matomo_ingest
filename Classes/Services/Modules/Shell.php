<?php

/**
 * @noinspection PhpUnusedLocalVariableInspection
 * @noinspection DuplicatedCode
 * @noinspection UnknownInspectionInspection
 */

namespace WEBprofil\MatomoIngest\Services\Modules;

use RuntimeException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use WEBprofil\MatomoIngest\Services\Matomo;

class Shell
{
    protected string $matomoUrl;
    protected string $authToken;
    public string|bool $siteId = '';
    protected string|bool $siteUrl;
    protected string $siteDomain;

    protected const PYTHON_BIN = '/usr/bin/python3';
    /** @noinspection SpellCheckingInspection */
    protected const PYTHONS_SCRIPT_FILE = '/vendor/webprofil/matomo-ingest/Private/Assets/import_logs.py';

    protected const LOG_FILE_NAME = '$(date -d "yesterday" +\%Y\%m\%d)-access.log';

    protected const LOG_PATH = '/log/';
    protected const LOG_PATH_FALLBACK = '../../log/';

    public function __construct(string &$matomoUrl, string &$authToken, string &$siteId, string &$siteUrl, string &$siteDomain)
    {
        $this->matomoUrl = &$matomoUrl;
        $this->authToken = &$authToken;
        $this->siteId = &$siteId;
        $this->siteUrl = &$siteUrl;
        $this->siteDomain = &$siteDomain;
    }

    public function runMatomoImportCommand(): string
    {
        /** @noinspection SpellCheckingInspection */
        $arguments = [
            '--token-auth=' . $this->authToken,
            '--idsite=' . $this->siteId,
            '--url=' . $this->matomoUrl,
        ];

        /** @noinspection NullPointerExceptionInspection */
        $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('matomo_ingest');

        if ($extConf['ignoreList']) {
            $ignoreList = GeneralUtility::trimExplode(',', $extConf['ignoreList']);
            foreach ($ignoreList as $ignorePath) {
                $ignorePath = trim($ignorePath);
                if (!$ignorePath) {
                    continue;
                }

                $arguments[] = '--exclude-path="' . $ignorePath . '"';
            }
        }

        foreach (['/typo3/*', '/index.php?eID=t3monitoring*'] as $excludePath) {
            $arguments[] = '--exclude-path="' . $excludePath . '"';
        }

        $logPath = self::LOG_PATH . self::LOG_FILE_NAME;
        [$stdOut, $stdErr, $returnCode] = self::runShellCommand('tail ' . $logPath);

        if ($returnCode !== 0) {
            $logPath = self::LOG_PATH_FALLBACK . self::LOG_FILE_NAME;

            [$stdOut, $stdErr, $returnCode] = self::runShellCommand('tail ' . $logPath);
            if ($returnCode !== 0) {
                throw new RuntimeException('Log file not found / no permissions, task only works when triggered by a cronjob.');
            }
        }

        $returnStr = '';
        $count = 0;

        // NOTE: registry instance created here, since it's only used in this method
        $registry = GeneralUtility::makeInstance(Registry::class);
        $fullImport = $registry->get(Matomo::REGISTRY_NAMESPACE, Matomo::REGISTRY_FULL_IMPORT_UNIX_TIMESTAMP) ?? false;

        $processLogDataToStr = static function (string $domain, int $status, string $stdOut, string $stdErr, string $logPath): string {
            $s = '';

            if ($status === 0) {
                $s .= '# > [success] - ' . $domain . ' - ' . $logPath . PHP_EOL;
                $s .= $stdOut . PHP_EOL;
            } else {
                $s .= '# > [error] - ' . $domain . ' - ' . $logPath . PHP_EOL;
                $s .= $stdErr . PHP_EOL;
            }

            return $s;
        };

        if ($fullImport) {
            // NOTE: import log file from yesterday

            $command = self::buildShellCommand($arguments, $logPath);

            [$stdOut, $stdErr, $returnCode] = self::runShellCommand($command);
            $returnStr .= $processLogDataToStr($this->siteDomain, $returnCode, $stdOut, $stdErr, $logPath);

            $count++;
        } else {
            // NOTE: import all log files except the current one

            $findCommand = 'find ' . str_replace(self::LOG_FILE_NAME, '', $logPath) . ' -type f | grep "access" | grep -v "/access.log"';

            [$stdOut, $stdErr, $returnCode] = self::runShellCommand($findCommand);

            foreach (explode("\n", $stdOut) as $logFile) {
                $logFile = trim($logFile);
                if (!$logFile) {
                    continue;
                }

                $command = self::buildShellCommand($arguments, $logFile);

                [$stdOut, $stdErr, $returnCode] = self::runShellCommand($command);
                $returnStr .= $processLogDataToStr($this->siteDomain, $returnCode, $stdOut, $stdErr, $logPath);

                $count++;
            }

            $registry->set(Matomo::REGISTRY_NAMESPACE, Matomo::REGISTRY_FULL_IMPORT_UNIX_TIMESTAMP, time());
        }

        $returnStr .= '# > [count] Processed ' . $count . ' log files.';

        return $returnStr;
    }

    protected static function buildShellCommand(array $arguments, string $logPath): string
    {
        return self::PYTHON_BIN . ' ' . Environment::getProjectPath() . self::PYTHONS_SCRIPT_FILE . ' ' . $logPath . ' ' . implode(' ', $arguments);
    }

    protected static function runShellCommand(string $command, ?string $cwd = null, ?array $env = null): array
    {
        /** @noinspection SpellCheckingInspection */
        $descriptorspec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"],
            2 => ["pipe", "w"],
        ];

        // open process
        $process = proc_open($command, $descriptorspec, $pipes, $cwd, $env);

        if (!is_resource($process)) {
            throw new RuntimeException('error opening process');
        }

        // close stdin pipe, no more input will be sent
        fclose($pipes[0]);

        $stdout = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);

        $returnCode = proc_close($process);

        return [
            $stdout,
            $stderr,
            $returnCode,
        ];
    }
}
