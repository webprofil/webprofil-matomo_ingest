<?php

/**
 * @noinspection DuplicatedCode
 * @noinspection PhpComposerExtensionStubsInspection
 * @noinspection UnknownInspectionInspection
 */

namespace WEBprofil\MatomoIngest\Services\Modules;

use RuntimeException;

class Http
{
    public static function buildRequestUrl($url, $query = [])
    {
        if ($query !== []) {
            $url .= '?';
            foreach ($query as $key => $value) {
                $url .= $key . '=' . $value . '&';
            }
        }

        return $url;
    }

    public static function fetchData(string $url, array $query = [], string $encoding = null): bool|array|string
    {
        $url = self::buildRequestUrl($url, $query);

        $c = curl_init();

        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, ['Accept-Encoding: utf-8']);

        $res = curl_exec($c);
        $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);

        if ($encoding) {
            return iconv($encoding, 'UTF-8', $res);
        }

        if ($status === 401) {
            throw new RuntimeException('Invalid Matomo Auth Token');
        }

        return [$res, $status];
    }
}
