<?php

/**
 * @noinspection UnknownInspectionInspection
 * @noinspection DuplicatedCode
 */

namespace WEBprofil\MatomoIngest\Services\Modules;

use JsonException;
use RuntimeException;

class Api
{
    protected string $matomoUrl;
    protected string $authToken;
    public string|bool $siteId = '';
    protected string|bool $siteUrl;
    protected string $siteDomain;

    public function __construct(string &$matomoUrl, string &$authToken, string &$siteId, string &$siteUrl, string &$siteDomain)
    {
        $this->matomoUrl = &$matomoUrl;
        $this->authToken = &$authToken;
        $this->siteId = &$siteId;
        $this->siteUrl = &$siteUrl;
        $this->siteDomain = &$siteDomain;
    }

    /**
     * @throws JsonException
     */
    public function getSiteIdFromUrl(?string $url = null)
    {
        $query = [
            'module' => 'API',
            'method' => 'SitesManager.getSitesIdFromSiteUrl',
            'token_auth' => $this->authToken,
            'url' => $url ?? $this->siteUrl,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        $jsonData = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        /** @noinspection SpellCheckingInspection */
        return $jsonData[0]['idsite'] ?? false;
    }

    /**
     * @throws JsonException
     */
    public function createSiteIdFromUrl()
    {
        $query = [
            'module' => 'API',
            'method' => 'SitesManager.addSite',
            'siteName' => $this->siteDomain,
            'urls' => $this->siteUrl,
            'type' => 'Website',
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        $jsonData = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        return $jsonData['value'] ?? false;
    }

    /**
     *
     * @throws JsonException
     */
    public function validateAndUpdateUser($md5Password): string | bool
    {
        if ($this->userExists()) {
            if (!$this->userIsValid()) {
                $this->deleteUser();
                $md5Password = $this->addUser();
                $this->setUserAccess();
            }
        } else {
            $md5Password = $this->addUser();
            $this->setUserAccess();
        }

        if (!$this->validUserCredentials($md5Password)) {
            $this->deleteUser();
            $md5Password = $this->addUser();
            $this->setUserAccess();
        }

        return $md5Password;
    }

    public function validUserCredentials($md5Password): bool
    {
        /** @noinspection SpellCheckingInspection */
        [$data, $status] = Http::fetchData($this->matomoUrl, [
            'module' => 'Login',
            'action' => 'logme',
            'login' => $this->siteDomain,
            'password' => $md5Password,
        ]);
        unset($data);

        return $status === 302;
    }

    /**
     * @throws JsonException
     */
    public function getUser()
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.getUser',
            'userLogin' => $this->siteDomain,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws JsonException
     */
    public function userExists()
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.userExists',
            'userLogin' => $this->siteDomain,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        $jsonData = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        return $jsonData['value'];
    }

    /**
     * @throws RuntimeException
     */
    public function addUser(): string
    {
        $newPassword = uniqid('', true);

        $query = [
            'module' => 'API',
            'method' => 'UsersManager.addUser',

            'userLogin' => $this->siteDomain,
            'password' => $newPassword,
            'email' => uniqid('generated_', true) . '@null.null',
            'initialIdSite' => '',
            'passwordConfirmation' => '',

            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        Http::fetchData($this->matomoUrl, $query);

        return md5($newPassword);
    }

    public function deleteUser(?string $username = null): void
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.deleteUser',
            'userLogin' => $username ?? $this->siteDomain,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        Http::fetchData($this->matomoUrl, $query);
    }

    public function setUserAccess(): void
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.setUserAccess',
            'userLogin' => $this->siteDomain,
            'access' => 'view',
            'idSites' => $this->siteId,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        Http::fetchData($this->matomoUrl, $query);
    }

    /**
     * @throws JsonException
     */
    public function userIsValid(): bool
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.getSitesAccessFromUser',
            'userLogin' => $this->siteDomain,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        $jsonData = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        $mappedData = [];
        foreach ($jsonData as $entry) {
            $mappedData[$entry['site']] = $entry['access'];
        }

        if (count($mappedData) !== 1) {
            return false;
        }

        if (@$mappedData[$this->siteId] !== 'view') {
            return false;
        }

        if ((string) $this->getUser()['superuser_access'] !== '0') {
            return false;
        }

        return true;
    }

    public function updateSiteUrl(string $siteId, string $url): void
    {
        $parsedUrl = parse_url($url);
        if ($parsedUrl === false) {
            throw new RuntimeException('Invalid Site URL');
        }
        $domain = $parsedUrl['host'];

        $query = [
            'module' => 'API',
            'method' => 'SitesManager.updateSite',
            'idSite' => $siteId,
            'siteName' => $domain,
            'urls' => $url,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($data);

        if ($status !== 200) {
            throw new RuntimeException('Site update failed');
        }
    }

    /**
     * @throws JsonException
     */
    public function deleteOldUsers(): void
    {
        $users = $this->getUsersWithSiteAccess();

        if (@$users['result'] === 'error') {
            return;
        }

        foreach ($users as $user) {
            if ($user['superuser_access'] !== 0) {
                continue;
            }

            if ($user['login'] !== $this->siteDomain) {
                $this->deleteUser($user['login']);
            }
        }
    }

    /**
     * @throws JsonException
     */
    public function getUsersWithSiteAccess()
    {
        $query = [
            'module' => 'API',
            'method' => 'UsersManager.getUsersWithSiteAccess',
            'idSite' => $this->siteId,
            'access' => 'view',
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws JsonException
     */
    public function getDataFromSiteId(?int $siteId = null): array | false
    {
        /** @noinspection ProperNullCoalescingOperatorUsageInspection */
        $query = [
            'module' => 'API',
            'method' => 'SitesManager.getSiteFromId',
            'idSite' => $siteId ?? $this->siteId,
            'token_auth' => $this->authToken,
            'format' => 'JSON',
        ];

        [$data, $status] = Http::fetchData($this->matomoUrl, $query);
        unset($status);

        $jsonData = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        if (@$jsonData['result'] === 'error') {
            return false;
        }

        return $jsonData;
    }
}
