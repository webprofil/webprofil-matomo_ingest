<?php

/**
 * @noinspection DuplicatedCode
 * @noinspection UnknownInspectionInspection
 */

namespace WEBprofil\MatomoIngest\Services;

use JsonException;
use MatomoTracker;
use RuntimeException;
use Throwable;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use WEBprofil\MatomoIngest\Services\Modules\Api;
use WEBprofil\MatomoIngest\Services\Modules\Shell;

class Matomo
{
    public Api $api;
    public Shell $shell;

    public MatomoTracker $tracker;

    public string $matomoUrl = '';
    public string|bool $siteId = '';
    public string|bool $siteUrl = '';
    public string $siteDomain = '';

    protected string $authToken = '';

    protected PersistenceManager $persistenceManager;

    public const REGISTRY_NAMESPACE = 'matomo_ingest';

    public const REGISTRY_PASSWORD = 'user_pwd';
    public const REGISTRY_SITE_ID = 'site_id';
    public const REGISTRY_SITE_URL = 'site_url';

    public const REGISTRY_FULL_IMPORT_UNIX_TIMESTAMP = 'full_import_unix_timestamp';

    /**
     * Initialize doesn't happen in the constructor because the Scheduler BE Module initializes the class on view.
     * Failing checks (site id / auth token) would lead to the Scheduler BE Module not being accessible.
     *
     * Needs to be called before any other method.
     *
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws ExtensionConfigurationExtensionNotConfiguredException|JsonException
     */
    public function init(bool $skipChecks = false): void
    {
        if (getenv('MATOMO_DISABLED')) {
            if (@$GLOBALS['TYPO3_REQUEST']) {
                DebuggerUtility::var_dump("Matomo is disabled via env:MATOMO_DISABLED");
            } else {
                echo "Matomo is disabled env:MATOMO_DISABLED\n";
            }
            exit (0);
        }

        // constructor variables are passed by reference
        $this->api = new Api($this->matomoUrl, $this->authToken, $this->siteId, $this->siteUrl, $this->siteDomain);
        $this->shell = new Shell($this->matomoUrl, $this->authToken, $this->siteId, $this->siteUrl, $this->siteDomain);

        $this->persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);

        /** @noinspection NullPointerExceptionInspection */
        $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('matomo_ingest');
        $registry = GeneralUtility::makeInstance(Registry::class);

        // set variables based on configuration / environment / registry
        // order is important since some variables depend on others

        $this->_initAuthToken($extConf);
        $this->_initMatomoUrl($extConf);

        $this->_initSiteUrl($extConf, $registry);

        // if (no site URL) and (site ID) -> check site ID -> get site URL from API -> update site URL
        if ($this->siteUrl === false && $this->siteId !== false) {
            $this->_initSiteId($extConf, $registry);
            if ($this->siteId !== false) {
                $apiSiteData = $this->api->getDataFromSiteId();

                if (isset($apiSiteData['main_url'])) {
                    $this->siteUrl = $apiSiteData['main_url'];
                    $registry->set(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_URL, $this->siteUrl);
                }
            }

            try {
                $hostUser = trim(get_current_user());
                $hostUser = strtolower($hostUser);

                $hostUser = str_replace(' ', '-', $hostUser);
                $hostUser = preg_replace('/[^A-Za-z0-9\-]/', '-', $hostUser);

                $hostUser = trim($hostUser, '-');
                $hostUser .= '.';
            } /** @noinspection PhpUnusedLocalVariableInspection */ catch (Throwable $e) {
                $hostUser = '';
            }

            if ($this->siteUrl === false) {
                $this->siteUrl = uniqid("https://generated.$hostUser", true) . '.null';
            }
        }

        $this->_initSiteDomain();

        // skip validation for event tracking
        if (!$skipChecks) {
            $this->_initSiteId($extConf, $registry);
            $this->_initUser($registry);
            $this->api->updateSiteUrl($this->siteId, $this->siteUrl);
        } else {
            $this->siteId = $registry->get(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_ID) ?? false;
        }

        $this->tracker = new MatomoTracker($this->siteId, $this->matomoUrl);
        $this->tracker = $this->tracker->setTokenAuth($this->authToken);
    }

    protected function _initAuthToken($extConf): void
    {
        $authToken = trim($extConf['authToken'] ?? '');
        $authTokenDisableOverride = trim($extConf['authTokenDisableOverride'] ?? '') === '1';
        if (!$authTokenDisableOverride) {
            $envAuthToken = trim(getenv('MATOMO_AUTH_TOKEN') ?? '');
            if ($envAuthToken) {
                $authToken = $envAuthToken;
            }

            if (!$authToken) {
                throw new RuntimeException('No Matomo Auth Token found');
            }
        }

        $this->authToken = $authToken;
    }

    protected function _initMatomoUrl($extConf): void
    {
        $matomoUrl = trim($extConf['matomoUrl'] ?? '');
        $matomoUrlDisableOverride = trim($extConf['matomoUrlDisableOverride'] ?? '') === '1';
        if (!$matomoUrlDisableOverride) {
            /** @noinspection SpellCheckingInspection */
            $envmatomoUrl = trim(getenv('MATOMO_URL') ?? '');
            if ($envmatomoUrl) {
                $matomoUrl = $envmatomoUrl;
            }

            if (!$matomoUrl) {
                throw new RuntimeException('No Matomo URL found');
            }
        }

        $this->matomoUrl = $matomoUrl;
    }

    protected function _initSiteUrl($extConf, $registry): void
    {
        /* @var string|bool $registrySiteUrl */
        $registrySiteUrl = $registry->get(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_URL) ?? false;

        $siteUrl = trim($extConf['siteUrl'] ?? '');
        $siteUrlDisableOverride = trim($extConf['siteUrlDisableOverride'] ?? '') === '1';
        if (!$siteUrlDisableOverride) {
            /** @noinspection SpellCheckingInspection */
            $envsiteUrl = trim(getenv('MATOMO_SITE_URL') ?? '');
            if ($envsiteUrl) {
                $siteUrl = $envsiteUrl;
            }

            if ($siteUrl && !str_starts_with($siteUrl, 'https://')) {
                throw new RuntimeException('Site URL must start with https://');
            }
        }

        $siteUrl = trim(trim($siteUrl, '/'));
        if ($siteUrl === '') {
            $siteUrl = false;
        }

        if (isset($_SERVER['HTTP_HOST']) && (!$siteUrl)) {
            $siteUrl = 'https://' . $_SERVER['HTTP_HOST'];
        }

        if ($siteUrl && ($registrySiteUrl !== $siteUrl)) {
            $registry->set(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_URL, $siteUrl);
        } else if ($registrySiteUrl) {
            $siteUrl = $registrySiteUrl;
        }

        $this->siteUrl = $siteUrl;
    }

    protected function _initSiteDomain(): void
    {
        $parsedUrl = parse_url($this->siteUrl);
        if ($parsedUrl === false) {
            throw new RuntimeException('Invalid Site URL');
        }

        $this->siteDomain = $parsedUrl['host'];
    }

    /**
     * @throws JsonException
     */
    protected function _initSiteId($extConf, $registry): void
    {
        $this->siteId = $registry->get(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_ID) ?? false;

        if ($this->api->getDataFromSiteId() === false) {
            $this->siteId = false;
        }

        $localSiteId = $this->siteId;

        $siteIdOverride = trim($extConf['siteIdOverride'] ?? false);
        if ($siteIdOverride) {
            if ($this->api->getDataFromSiteId((int) $siteIdOverride) === false) {
                throw new RuntimeException('Invalid Site ID specified in configuration');
            }

            $localSiteId = $siteIdOverride;
        }

        if (!$localSiteId) {
            $localSiteId = $this->api->createSiteIdFromUrl();
        }

        if ($this->siteId !== $localSiteId) {
            $this->siteId = $localSiteId;
            $registry->set(self::REGISTRY_NAMESPACE, self::REGISTRY_SITE_ID, $this->siteId);
        }
    }

    /**
     * @throws JsonException
     */
    protected function _initUser($registry): void
    {
        $md5Password = $registry->get(self::REGISTRY_NAMESPACE, self::REGISTRY_PASSWORD) ?? '';
        $newMd5Password = $this->api->validateAndUpdateUser($md5Password);
        if ($newMd5Password !== $md5Password) {
            $registry->set(self::REGISTRY_NAMESPACE, self::REGISTRY_PASSWORD, $newMd5Password);
        }
    }
}
