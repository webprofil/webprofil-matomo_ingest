<?php

/**
 * @noinspection UnknownInspectionInspection
 * @noinspection DuplicatedCode
 */

namespace WEBprofil\MatomoIngest\Commands;

use Throwable;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use WEBprofil\MatomoIngest\Services\Matomo;

class MatomoImport extends Command
{
    protected Matomo $matomo;

    public function __construct()
    {
        $this->matomo = GeneralUtility::makeInstance(Matomo::class);
        parent::__construct();
    }

    /**
     * @throws Exception
     * @noinspection PhpComposerExtensionStubsInspection
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        $this->matomo->init();

        $this->matomo->api->deleteOldUsers();
        $out = $this->matomo->shell->runMatomoImportCommand();

        /** @noinspection NullPointerExceptionInspection */
        $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('matomo_ingest');

        if ($extConf['slackWebhook'] !== '') {
            $url = $extConf['slackWebhook'];
            /** @noinspection JsonEncodingApiUsageInspection */
            $jsonData = json_encode(['text' => $out]);

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData),
            ));

            try {
                curl_exec($ch);
            } /** @noinspection PhpUnusedLocalVariableInspection */ catch (Throwable $th) {
            }
        }

        $output->writeln($out);
        return Command::SUCCESS;
    }
}
